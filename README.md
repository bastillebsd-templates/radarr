## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/radarr/badges/main/pipeline.svg)](https://gitlab.com/bastillebsd-templates/radarr/commits/main)

## Radarr
Bastille Template for Radarr

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/radarr
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/radarr
```

